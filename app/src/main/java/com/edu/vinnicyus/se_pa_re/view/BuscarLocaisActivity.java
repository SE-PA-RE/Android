package com.edu.vinnicyus.se_pa_re.view;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.edu.vinnicyus.se_pa_re.R;
import com.edu.vinnicyus.se_pa_re.request.HttpGlobalRetrofit;
import com.edu.vinnicyus.se_pa_re.request.api.SolicitacaoColetaInterface;
import com.edu.vinnicyus.se_pa_re.request.deserializer.DirectionsDes;
import com.edu.vinnicyus.se_pa_re.request.models.response.DirectionsRes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;

public class BuscarLocaisActivity extends AppCompatActivity implements  OnMapReadyCallback, GoogleMap.OnMyLocationButtonClickListener,GoogleMap.OnMyLocationClickListener,GoogleMap.OnMarkerClickListener,LocationListener {

    private GoogleMap mMap;
    private LocationManager lm;

    private Marker ponto1;
    private Marker ponto2;
    private Marker ponto3;
    private Marker ponto4;

    private Polyline polyline;

    private LatLng location;
    private Marker marker_atual;

    private View view_rota;

    private AppCompatTextView info_user_text;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.locais:
                    moveCameraLocais();
                    break;

            }
            return true;
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buscar_locais);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        BottomNavigationView navigationView = findViewById(R.id.navigation);
        navigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        info_user_text = findViewById(R.id.info_user_text);
        view_rota = findViewById(R.id.content_view_rota);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.toolbar_solicita_coleta, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_location:
                startGettingLocations();
                return true;
            case android.R.id.home:
                finish();
                return true;
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        startGettingLocations();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        lm.removeUpdates(this);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {


        // Add a marker in Sydney and move the camera
        mMap = googleMap;
        LatLng sydney = new LatLng(-10.204136, -48.336620);
        LatLng sydney2 = new LatLng(-10.176939, -48.328827);
        LatLng sydney3 = new LatLng(-10.184649, -48.326767);
        LatLng sydney4 = new LatLng(-10.209742, -48.323254);

        ponto1 = mMap.addMarker(new MarkerOptions().position(sydney).snippet("Entrega ate 12h").title("Ponto de entrega 1").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
        ponto1.setTag(0);
        ponto2 = mMap.addMarker(new MarkerOptions().position(sydney2).title("Ponto de entrega 2"));
        ponto2.setTag(0);
        ponto3 = mMap.addMarker(new MarkerOptions().position(sydney3).title("Ponto de entrega 3"));
        ponto3.setTag(0);
        ponto4 = mMap.addMarker(new MarkerOptions().position(sydney4).title("Ponto de entrega 4"));
        ponto4.setTag(0);

        mMap.setOnMarkerClickListener(this);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 12));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.setOnMyLocationButtonClickListener(this);
        mMap.setOnMyLocationClickListener((GoogleMap.OnMyLocationClickListener) this);

    }

    @Override
    public void onMyLocationClick(@NonNull Location location) {
        Toast.makeText(this, "Current location:\n" + location, Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onMyLocationButtonClick() {
        Toast.makeText(this, "MyLocation button clicked", Toast.LENGTH_SHORT).show();
        // Return false so that we don't consume the event and the default behavior still occurs
        // (the camera animates to the user's current position).
        return false;
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        marker_atual = marker;
        printRouter(marker);
        view_rota.setVisibility(View.VISIBLE);
        return false;
    }

    public void closeView(View view){
        view_rota.setVisibility(View.GONE);
    }

    @NonNull
    private String latLng(LatLng latLng){
        return latLng.latitude+","+latLng.longitude;
    }

//    private void getRouter(LatLng origin, LatLng destination){
//        String url = "https://maps.googleapis.com/maps/api/directions/";
//        Gson gson = new GsonBuilder().registerTypeAdapter(DirectionsRes.class, new DirectionsDes()).create();
//        HttpGlobalRetrofit retrofit = new HttpGlobalRetrofit(url,gson);
//        SolicitacaoColetaInterface solicitacaoColetaInterface = retrofit.getRetrofit().create(SolicitacaoColetaInterface.class);
//        Call<DirectionsRes> call = solicitacaoColetaInterface.getRotas(latLng(origin), latLng(destination), "AIzaSyDKQYbJ3vlcAy-0KvRZHluBlScq2F6PQCI");
//        call.enqueue(new Callback<DirectionsRes>() {
//            @Override
//            public void onResponse(Call<DirectionsRes> call, retrofit2.Response<DirectionsRes> response) {
//                if(response.code() == 200){
//                    setRouters(response.body());
//                }else{
//
//                }
//            }
//
//            @Override
//            public void onFailure(Call<DirectionsRes> call, Throwable t) {
//
//            }
//        });
//    }

    private void setRouters(DirectionsRes directionsRes){
        //PolylineOptions reactOption = new PolylineOptions().add(sydney).add(sydney2).add(sydney3).add(sydney4);
        //polyline = mMap.addPolyline(reactOption);
    }

    private void printRouter(Marker marker){
        TextView distancia = findViewById(R.id.distancia);
        TextView title = findViewById(R.id.title_ponto);
        title.setText(marker.getTitle());
        distancia.setText(marker.getSnippet());
        Button btn_rota = findViewById(R.id.btn_rota);
        final Marker m = marker;
        btn_rota.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String geo = "google.navigation:q="+m.getPosition().latitude+","+m.getPosition().longitude;

                Uri geoUri = Uri.parse( geo );
                Intent intent = new Intent( Intent.ACTION_VIEW, geoUri );

                /*
                 * Aqui estamos definindo que: se o aplicativo Google Maps estiver
                 * presente no aparelho, que ele seja utilizado para a apresentação
                 * do local em intent. Caso contrário qualquer aplicativo, que responda
                 * a Intent criada, pode ser utilizado.
                 * */
                intent.setPackage( "com.google.android.apps.maps" );

                startActivity( intent );
            }
        });
    }


    private void moveCameraLocais(){
        CameraPosition cameraPosition = new CameraPosition.Builder().zoom(12).target(ponto3.getPosition()).build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    @Override
    public void onLocationChanged(Location loca_atual) {
        location = new LatLng(loca_atual.getLatitude(),loca_atual.getLongitude());
        mMap.addCircle(new CircleOptions().center(location));
        CameraPosition cameraPosition = new CameraPosition.Builder().zoom(13).target(location).build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        info_user_text.setVisibility(View.GONE);
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    private ArrayList findUnAskedPermissions(ArrayList<String> wanted) {
        ArrayList result = new ArrayList();

        for (String perm : wanted) {
            if (!hasPermission(perm)) {
                result.add(perm);
            }
        }

        return result;
    }

    private boolean hasPermission(String permission) {
        if (canAskPermission()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    }

    private boolean canAskPermission() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("GPS desativado!");
        alertDialog.setMessage("Ativar GPS?");
        alertDialog.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        });

        alertDialog.setNegativeButton("Não", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        alertDialog.show();
    }

    private void startGettingLocations() {
        info_user_text.setVisibility(View.VISIBLE);
        lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        boolean isGPS = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean isNetwork = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        boolean canGetLocation = true;
        int ALL_PERMISSIONS_RESULT = 101;
        long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10;// Distance in meters
        long MIN_TIME_BW_UPDATES = 1000 * 10;// Time in milliseconds

        ArrayList<String> permissions = new ArrayList<>();
        ArrayList<String> permissionsToRequest;

        permissions.add(android.Manifest.permission.ACCESS_FINE_LOCATION);
        permissions.add(android.Manifest.permission.ACCESS_COARSE_LOCATION);
        permissionsToRequest = findUnAskedPermissions(permissions);

        //Check if GPS and Network are on, if not asks the user to turn on
        if (!isGPS && !isNetwork) {
            showSettingsAlert();
        } else {
            // check permissions

            // check permissions for later versions
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (permissionsToRequest.size() > 0) {
                    requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]),
                            ALL_PERMISSIONS_RESULT);
                    canGetLocation = false;
                }
            }
        }


        //Checks if FINE LOCATION and COARSE Location were granted
        if (ActivityCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(this, "Permissão negada", Toast.LENGTH_SHORT).show();
            return;
        }

        //Starts requesting location updates
        if (canGetLocation) {
            if (isGPS) {
                lm.requestLocationUpdates(
                        LocationManager.GPS_PROVIDER,
                        MIN_TIME_BW_UPDATES,
                        MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

            } else if (isNetwork) {
                // from Network Provider

                lm.requestLocationUpdates(
                        LocationManager.NETWORK_PROVIDER,
                        MIN_TIME_BW_UPDATES,
                        MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

            }
        } else {
            Toast.makeText(this, "Não é possível obter a localização", Toast.LENGTH_SHORT).show();
        }
    }

}
