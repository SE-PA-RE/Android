package com.edu.vinnicyus.se_pa_re.request.models.response;

import com.edu.vinnicyus.se_pa_re.models.Usuario;

import java.util.ArrayList;

public class ListSolicitacoesResModel {

    private ArrayList<Solicitacao> pendentes;
    private ArrayList<Solicitacao> finalizados_emAtendimento;

    public ArrayList<Solicitacao> getPendentes() {
        return pendentes;
    }

    public ArrayList<Solicitacao> getFinalizados_emAtendimento() {
        return finalizados_emAtendimento;
    }

    public class Solicitacao{

        private int id;
        private String status;
        private Coleta coleta;
        private String created_at;
        private String updated_at;

        public int getId() {
            return id;
        }

        public String getStatus() {
            return status;
        }

        public Coleta getColeta() {
            return coleta;
        }

        public String getCreated_at() {
            return created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public class Coleta{

            private int id;
            private Agente agente;
            private String created_at;
            private String updated_at;

            public int getId() {
                return id;
            }

            public Agente getAgente() {
                return agente;
            }

            public String getCreated_at() {
                return created_at;
            }

            public String getUpdated_at() {
                return updated_at;
            }

            public class Agente{
                private int id;
                private Usuario usuario;
                private int rg;
                private String cpf;

                public int getId() {
                    return id;
                }

                public Usuario getUsuario() {
                    return usuario;
                }

                public int getRg() {
                    return rg;
                }

                public String getCpf() {
                    return cpf;
                }
            }
        }
    }

}
