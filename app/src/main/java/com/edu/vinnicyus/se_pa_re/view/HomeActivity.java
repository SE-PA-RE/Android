package com.edu.vinnicyus.se_pa_re.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.edu.vinnicyus.se_pa_re.IndexActivity;
import com.edu.vinnicyus.se_pa_re.R;
import com.edu.vinnicyus.se_pa_re.models.Token;

import io.realm.Realm;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        Button sair = findViewById(R.id.sair);
        sair.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                logout();
            }
        });

        LinearLayout solicitar_coleta = findViewById(R.id.solicita_coleta_Linear);
        solicitar_coleta.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeActivity.this, SolicitarColetaActivity.class);
                startActivity(intent);
            }
        });

        LinearLayout historico = findViewById(R.id.historico);
        historico.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeActivity.this, HistoricoActivity.class);
                startActivity(intent);
            }
        });

        LinearLayout conta = findViewById(R.id.conta);
        conta.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeActivity.this, EditUserActivity.class);
                startActivity(intent);
            }
        });

        LinearLayout buscar_locais = findViewById(R.id.buscar_locais);
        buscar_locais.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeActivity.this, BuscarLocaisActivity.class);
                startActivity(intent);
            }
        });

        LinearLayout materiais_registro = findViewById(R.id.materiais_registro);
        materiais_registro.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeActivity.this, MateriaisActivity.class);
                startActivity(intent);
            }
        });
    }

    private void logout(){
        Realm realm = Realm.getDefaultInstance();
        Token t = Token.getLoginAttemp();
        realm.beginTransaction();
        t.setAccess_token(null);
        t.setRefresh_token(null);
        t.setTime(0);
        realm.commitTransaction();
        realm.close();

        Intent intent = new Intent(this, IndexActivity.class);
        startActivity(intent);
        finish();
    }
}
