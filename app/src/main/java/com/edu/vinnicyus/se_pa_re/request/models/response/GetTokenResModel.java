package com.edu.vinnicyus.se_pa_re.request.models.response;

import java.sql.Timestamp;
import java.util.ArrayList;

public class GetTokenResModel {

    private String access_token;
    private String refresh_token;
    private long expires_in;
    private String token_type;
    private Timestamp updated_at;

    public GetTokenResModel(){ }

    public String getAcess_token() {
        return access_token;
    }

    public void setAcess_token(String acess_token) {
        this.access_token = acess_token;
    }

    public String getRefresh_token() {
        return refresh_token;
    }

    public void setRefresh_token(String refresh_token) {
        this.refresh_token = refresh_token;
    }

    public long getExpiracao() {
        return expires_in;
    }

    public void setExpiracao(long expiracao) {
        this.expires_in = expiracao;
    }

    public String getType_credentials() {
        return token_type;
    }

    public void setType_credentials(String type_credentials) {
        this.token_type = type_credentials;
    }

    public Timestamp getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(Timestamp updated_at) {
        this.updated_at = updated_at;
    }

    public class Errors{

        private boolean status;

        public boolean isStatus() {
            return status;
        }

        public void setStatus(boolean status) {
            this.status = status;
        }
    }
}

