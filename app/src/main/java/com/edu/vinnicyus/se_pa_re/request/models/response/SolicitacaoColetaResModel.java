package com.edu.vinnicyus.se_pa_re.request.models.response;

import java.util.ArrayList;

public class SolicitacaoColetaResModel {

    private String message;
    private boolean status;
    private Errors errors;


    public SolicitacaoColetaResModel(String message, boolean status, Errors errors) {
        this.message = message;
        this.status = status;
        this.errors = errors;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Errors getErrors() {
        return errors;
    }

    public void setErrors(Errors errors) {
        this.errors = errors;
    }

    public class Errors{

        private ArrayList longitude;
        private ArrayList latitude;

        public ArrayList getLongitude() {
            return longitude;
        }

        public void setLongitude(ArrayList longitude) {
            this.longitude = longitude;
        }

        public ArrayList getLatitude() {
            return latitude;
        }

        public void setLatitude(ArrayList latitude) {
            this.latitude = latitude;
        }
    }
}
