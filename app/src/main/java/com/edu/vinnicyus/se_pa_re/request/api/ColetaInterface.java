package com.edu.vinnicyus.se_pa_re.request.api;

import com.edu.vinnicyus.se_pa_re.request.models.request.StoreColetaModel;
import com.edu.vinnicyus.se_pa_re.request.models.response.MaterialUnidadeResModel;
import com.edu.vinnicyus.se_pa_re.request.models.response.StoreColetaRes;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ColetaInterface {

    @GET("api/coleta/getMateriaisUnidade")
    Call<MaterialUnidadeResModel> getMaterialUnidade();

    @POST("api/coleta/storeColetaMateriais")
    Call<StoreColetaRes> storeColeta(@Body StoreColetaModel scm);
}
