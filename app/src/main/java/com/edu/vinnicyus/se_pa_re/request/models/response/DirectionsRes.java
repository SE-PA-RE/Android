package com.edu.vinnicyus.se_pa_re.request.models.response;

import java.util.List;

public class DirectionsRes {

    private List<WayPoints> geocoded_waypoints;
    private List<Routes> routes;
    private String status;

    public List<WayPoints> getGeocoded_waypoints() {
        return geocoded_waypoints;
    }

    public List<Routes> getRoutes() {
        return routes;
    }

    public String getStatus() {
        return status;
    }

    public class WayPoints{

        private String geocoder_status;
        private String place_id;
        private List<String> types;

        public String getGeocoder_status() {
            return geocoder_status;
        }

        public String getPlace_id() {
            return place_id;
        }

        public List<String> getTypes() {
            return types;
        }
    }

    public class Routes{

        private List<Legs> legs;

        public List<Legs> getLegs() {
            return legs;
        }

        public class Legs{

            private List<Steps> steps;
            private Distance distance;
            private Duration duration;
            private StartLocaiton start_location;
            private EndLocation end_location;

            public List<Steps> getSteps() {
                return steps;
            }

            public Distance getDistance() {
                return distance;
            }

            public Duration getDuration() {
                return duration;
            }

            public StartLocaiton getStart_location() {
                return start_location;
            }

            public EndLocation getEnd_location() {
                return end_location;
            }

            public class Steps{

                private Distance distance;
                private Duration duration;
                private EndLocation end_location;
                private StartLocaiton start_location;

                public Distance getDistance() {
                    return distance;
                }

                public Duration getDuration() {
                    return duration;
                }

                public EndLocation getEnd_location() {
                    return end_location;
                }

                public StartLocaiton getStart_location() {
                    return start_location;
                }
            }

            public class StartLocaiton{

                private double lat;
                private double lng;

                public double getLat() {
                    return lat;
                }

                public double getLng() {
                    return lng;
                }
            }

            public class EndLocation{

                private double lat;
                private double lng;

                public double getLat() {
                    return lat;
                }

                public double getLng() {
                    return lng;
                }
            }


            public class Duration{
                private String text;
                private int value;

                public String getText() {
                    return text;
                }

                public int getValue() {
                    return value;
                }
            }
            public class Distance{
                private String text;
                private int value;

                public String getText() {
                    return text;
                }

                public int getValue() {
                    return value;
                }
            }
        }
    }
}
