package com.edu.vinnicyus.se_pa_re.request.api;

import com.edu.vinnicyus.se_pa_re.request.models.request.SolicitacaoColetaModel;
import com.edu.vinnicyus.se_pa_re.request.models.response.DirectionsRes;
import com.edu.vinnicyus.se_pa_re.request.models.response.ListSolicitacoesResModel;
import com.edu.vinnicyus.se_pa_re.request.models.response.SolicitacaoColetaResModel;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface SolicitacaoColetaInterface {

    @POST("api/solicitacao_coleta")
    Call<SolicitacaoColetaResModel> solicitarColeta(@Body SolicitacaoColetaModel solicitacaoColetaModel);

    @GET("api/solicitacao_coleta")
    Call<ListSolicitacoesResModel> getSolicitacoes();

    @GET("json")
    Call<DirectionsRes> getRotas(@Query("origin") String origin, @Query("destination") String destination, @Query("key") String key);
}
