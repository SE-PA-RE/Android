package com.edu.vinnicyus.se_pa_re.models;

import android.support.annotation.Nullable;

import java.sql.Timestamp;
import java.util.Calendar;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;

public class Token extends RealmObject {

    private String access_token;

    private String refresh_token;

    private long time;

    private String updated_at;

    private int identificar;

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getRefresh_token() {
        return refresh_token;
    }

    public void setRefresh_token(String refresh_token) {
        this.refresh_token = refresh_token;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public int getIdentificar() {
        return identificar;
    }

    public void setIdentificar(int identificar) {
        this.identificar = identificar;
    }

//    public static boolean checkAuth(){
//        List<Token> t = new Select().from(Token.class).where("id = ?",1).execute();
//        return t.isEmpty();
//    }
//
    @Nullable
    public static Token getLoginAttemp(){
        Realm realm = Realm.getDefaultInstance();
        RealmResults<Token> token = realm.where(Token.class).equalTo("identificar", 2).findAll();
        return token.size() == 0 ? null : token.first();
    }

}
