package com.edu.vinnicyus.se_pa_re.request;


import com.google.gson.Gson;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class HttpGlobalRetrofit {

    private final Retrofit retrofit;

    //private static final String ENDERECO_SERVIDOR="http://10.1.1.3/separe/public/";
    private static final String ENDERECO_SERVIDOR="https://campusparaiso.ifto.edu.br/separe/public/";

    public HttpGlobalRetrofit() {
        retrofit = new Retrofit.Builder().baseUrl(ENDERECO_SERVIDOR).addConverterFactory(GsonConverterFactory.create())
                .client(aplicarInterceptor())
                .build();
    }

    public HttpGlobalRetrofit(String Urlpersonalizada, Gson gson) {
        retrofit = new Retrofit.Builder().baseUrl(Urlpersonalizada).addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }

    public HttpGlobalRetrofit(Gson gson){
        retrofit = new Retrofit.Builder()
                .baseUrl(ENDERECO_SERVIDOR)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(aplicarInterceptor())
                .build();
    }

    private OkHttpClient aplicarInterceptor(){
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(new HttpInterceptor())
                .build();
        return client;
    }

    public Retrofit getRetrofit() {
        return retrofit;
    }
}
