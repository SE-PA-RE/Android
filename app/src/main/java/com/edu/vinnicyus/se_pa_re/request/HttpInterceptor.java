package com.edu.vinnicyus.se_pa_re.request;

import com.edu.vinnicyus.se_pa_re.models.Token;

import java.io.IOException;

import io.realm.Realm;
import io.realm.RealmResults;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class HttpInterceptor implements Interceptor{

    private static final String HEADER_AUTORIZACAO="Authorization";
    private static final String TIPO_TOKEN="Bearer ";
    public HttpInterceptor() { }

    @Override
    public Response intercept(Interceptor.Chain chain) throws IOException {
        Request requisicao=chain.request();
        Request.Builder construtorRequisicao=requisicao.newBuilder();
        construtorRequisicao.addHeader("Accept","application/json");
        Token t = getToken();
        if(t != null) {
            if (t.getAccess_token() != null) {
                construtorRequisicao.addHeader(HEADER_AUTORIZACAO, TIPO_TOKEN + t.getAccess_token());
            }
        }
        Request novaRequisicao=construtorRequisicao.build();
        return chain.proceed(novaRequisicao);
    }

    private Token getToken(){
        Realm realm = Realm.getDefaultInstance();
        RealmResults<Token> token;
        token = realm.where(Token.class).equalTo("identificar", 2).findAll();
        return token.size() == 0 ? null : token.first();
    }

}
