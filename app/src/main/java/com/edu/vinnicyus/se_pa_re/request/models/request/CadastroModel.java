package com.edu.vinnicyus.se_pa_re.request.models.request;

public class CadastroModel {

    private String name;
    private String email;
    private String password;
    private String role;

    public CadastroModel(String mname, String memail, String mpassword, String mrole){
        name = mname;
        email = memail;
        password = mpassword;
        role = mrole;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
