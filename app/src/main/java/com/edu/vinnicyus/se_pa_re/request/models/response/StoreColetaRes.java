package com.edu.vinnicyus.se_pa_re.request.models.response;

public class StoreColetaRes {

    private String message;
    private String status;

    public String getMessage() {
        return message;
    }

    public String getStatus() {
        return status;
    }
}
