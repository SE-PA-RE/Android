package com.edu.vinnicyus.se_pa_re;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class SplashScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                next();
            }
        }, 2000);
    }

    private void next(){
        Intent intent = new Intent(SplashScreenActivity.this,
                IndexActivity.class);
        startActivity(intent);
        finish();
    }

}
