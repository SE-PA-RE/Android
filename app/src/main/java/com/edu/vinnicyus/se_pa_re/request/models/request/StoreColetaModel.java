package com.edu.vinnicyus.se_pa_re.request.models.request;

import java.util.ArrayList;

public class StoreColetaModel {

    private Localidade localidade;
    private ArrayList<Coleta> coleta;
    private int solicitacao_coleta_id;

    public StoreColetaModel() {
        this.localidade = new Localidade();
        this.coleta = new ArrayList<>();
    }

    public void setColeta(ArrayList<Coleta> coleta) {
        this.coleta = coleta;
    }

    public void setLocalidade(double latitude, double longitude) {
        this.localidade.setLatitude(latitude);
        this.localidade.setLongitude(longitude);
    }

    public ArrayList<Coleta> getColeta() {
        return coleta;
    }

    public void setSolicitacao_coleta_id(int solicitacao_coleta_id) {
        this.solicitacao_coleta_id = solicitacao_coleta_id;
    }

    public class Localidade{

        private double longitude;
        private double latitude;

        public void setLongitude(double longitude) {
            this.longitude = longitude;
        }

        public void setLatitude(double latitude) {
            this.latitude = latitude;
        }
    }

    public class Coleta{

        private String material_id;
        private String tipo_unidade_id;
        private float quantidade;

        public Coleta(String material_id, String tipo_unidade_id, float quantidade) {
            this.material_id = material_id;
            this.tipo_unidade_id = tipo_unidade_id;
            this.quantidade = quantidade;
        }

        public void setMaterial_id(String material_id) {
            this.material_id = material_id;
        }

        public void setTipo_unidade_id(String tipo_unidade_id) {
            this.tipo_unidade_id = tipo_unidade_id;
        }

        public void setQuantidade(float quantidade) {
            this.quantidade = quantidade;
        }
    }
}
