package com.edu.vinnicyus.se_pa_re.request.deserializer;

import com.edu.vinnicyus.se_pa_re.request.models.response.ListSolicitacoesResModel;
import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

public class ListSolicitacoesDes implements JsonDeserializer<Object> {
    @Override
    public Object deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonElement token = json.getAsJsonObject();

        return (new Gson().fromJson( token, ListSolicitacoesResModel.class));
    }
}
