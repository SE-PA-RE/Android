package com.edu.vinnicyus.se_pa_re;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.support.annotation.Nullable;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.edu.vinnicyus.se_pa_re.controllers.ProgressViewController;
import com.edu.vinnicyus.se_pa_re.models.Token;
import com.edu.vinnicyus.se_pa_re.request.HttpGlobalRetrofit;
import com.edu.vinnicyus.se_pa_re.request.deserializer.ClientCredentialsDes;
import com.edu.vinnicyus.se_pa_re.request.deserializer.TokenDes;
import com.edu.vinnicyus.se_pa_re.request.api.TokenInterface;
import com.edu.vinnicyus.se_pa_re.request.models.response.ClientCredentialsModel;
import com.edu.vinnicyus.se_pa_re.request.models.request.GetTokenModel;
import com.edu.vinnicyus.se_pa_re.request.models.response.GetTokenResModel;
import com.edu.vinnicyus.se_pa_re.validator.ValidatorCodesResponse;
import com.edu.vinnicyus.se_pa_re.view.HomeActivity;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;


/**
 * A login screen that offers login via email/password.
 */
public class LoginFragment extends Fragment {


    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private View mProgressView;
    private View formLogin;
    private ProgressViewController pc;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);

        // Set up the login form.
        mEmailView = (AutoCompleteTextView) view.findViewById(R.id.email);
        mProgressView = view.findViewById(R.id.login_progress);
        formLogin = view.findViewById(R.id.email_login_form);

        mPasswordView = (EditText) view.findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                    ((InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE))
                            .hideSoftInputFromWindow(mPasswordView.getWindowToken(), 0);
                    pc.show(true);
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        Button mEmailSignInButton = view.findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                ((InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE))
                        .hideSoftInputFromWindow(mPasswordView.getWindowToken(), 0);
                pc.show(true);
                attemptLogin();
                //testedegin();
            }
        });

        pc = new ProgressViewController(formLogin,mProgressView, getResources());
        return view;
    }
    private void testedegin(){
        Intent intent = new Intent(getContext(),HomeActivity.class);
        startActivity(intent);
        getActivity().finish();
    }
    private void attemptLogin(){

        String login = mEmailView.getText().toString();
        String senha = mPasswordView.getText().toString();

        mEmailView.setError(null);
        mPasswordView.setError(null);

        boolean cancel = false;
        View focusView = null;

        if(!isEmailValid(login)){
            cancel = true;
            focusView = mEmailView;
            mEmailView.setError(getString(R.string.error_invalid_email));
        }
        if(!isPasswordValid(senha)){
            cancel = true;
            focusView = mPasswordView;
            mPasswordView.setError(getString(R.string.error_invalid_password));
        }

        if(cancel){
            pc.show(false);
            focusView.requestFocus();
        }else{
            CredentialsTask(login,senha);
        }
    }
    public void UserLoginTask(String email, String senha, ClientCredentialsModel ccm) {

        Gson gson  = new GsonBuilder().registerTypeAdapter( GetTokenResModel.class, new TokenDes()).create();

        HttpGlobalRetrofit retrofit = new HttpGlobalRetrofit();

        TokenInterface tokenInterface = retrofit.getRetrofit().create(TokenInterface.class);

        GetTokenModel getT = new GetTokenModel();
        getT.setGrant_type("password");
        getT.setClient_id(""+ccm.getClient_id());
        getT.setClient_secret(ccm.getClient_secret());
        getT.setUsername(email);
        getT.setPassword(senha);
        getT.setApi("usuario");

        Call<GetTokenResModel> call = tokenInterface.createToken(getT);
        call.enqueue(new Callback<GetTokenResModel>() {
            @Override
            public void onResponse(Call<GetTokenResModel> call, retrofit2.Response<GetTokenResModel> response) {
                if (response.code() == 200) {
                    Realm realm = Realm.getDefaultInstance();
                    try {
                        Token t = Token.getLoginAttemp();
                        realm.beginTransaction();
                        t.setAccess_token(response.body().getAcess_token());
                        t.setRefresh_token(response.body().getRefresh_token());
                        t.setTime(response.body().getExpiracao());
                        realm.commitTransaction();
                    } finally {
                        realm.close();
                    }
                    pc.show(false);
                    Intent intent = new Intent(getContext(),
                            HomeActivity.class);
                    startActivity(intent);
                    getActivity().finish();
                }else if(response.code() == 422) {
                    mPasswordView.setError( "Credenciais informadas invalidas!");
                    mEmailView.setError( "Credenciais informadas invalidas!");
                }else{
                    Toast.makeText(getContext(), "Error de Conexão!", Toast.LENGTH_SHORT).show();
                }
                pc.show(false);
            }

            @Override
            public void onFailure(Call<GetTokenResModel> call, Throwable t) {
                Toast.makeText(getContext(), "Falha de conexão!", Toast.LENGTH_LONG).show();
                pc.show(false);
            }
        });
    }

    public void CredentialsTask(final String email, final String senha) {
        Gson gson  = new GsonBuilder().registerTypeAdapter( ClientCredentialsModel.class, new ClientCredentialsDes()).create();

        HttpGlobalRetrofit retrofit = new HttpGlobalRetrofit();

        TokenInterface tokenInterface = retrofit.getRetrofit().create(TokenInterface.class);

        Call<ClientCredentialsModel> call = tokenInterface.getCredentials();
        call.enqueue(new Callback<ClientCredentialsModel>() {
            @Override
            public void onResponse(Call<ClientCredentialsModel> call, retrofit2.Response<ClientCredentialsModel> response) {
                ClientCredentialsModel credentials = null;
                if(response.body() !=null) {
                    credentials = response.body();
                    if(!credentials.getClient_secret().isEmpty()) {
                        UserLoginTask(email,senha,credentials);
                    }else{
                        mPasswordView.setError("Error de conexão!");
                        mPasswordView.requestFocus();
                        pc.show(false);
                    }
                }else{
                    //Toast.makeText(getContext(), "Falha de conexão", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ClientCredentialsModel> call, Throwable t) {
                Toast.makeText(getContext(), "Falha de conexão", Toast.LENGTH_LONG).show();
                pc.show(false);
            }
        });
    }
    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }
}

