package com.edu.vinnicyus.se_pa_re.request.models.response;

import java.util.ArrayList;

public class CadastroResModel {

    private String message;
    private boolean status;
    private Errors errors;


    public CadastroResModel(String message, boolean status, Errors errors) {
        this.message = message;
        this.status = status;
        this.errors = errors;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Errors getErrors() {
        return errors;
    }

    public void setErrors(Errors errors) {
        this.errors = errors;
    }

    public class Errors{
        private ArrayList name;
        private ArrayList email;
        private ArrayList password;

        public ArrayList getName() {
            return name;
        }

        public void setName(ArrayList name) {
            this.name = name;
        }

        public ArrayList getEmail() {
            return email;
        }

        public void setEmail(ArrayList email) {
            this.email = email;
        }

        public ArrayList getPassword() {
            return password;
        }

        public void setPassword(ArrayList password) {
            this.password = password;
        }
    }
}
