package com.edu.vinnicyus.se_pa_re.request.models;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class GeocodingJson {

    private String estado;
    private String cidade;
    private String cep;
    private String bairro;
    private String rua;
    private String numero;

    private JSONObject dados;
    private boolean status;
    private JSONObject adress;

    public GeocodingJson(JSONObject geo){
        this.dados = geo;
        this.tratamento();
    }

    private void tratamento(){
        try{
            if(this.dados.getString("status").equals("OK")) {
                this.status = true;
            }else {
                this.status = false;
            }
            JSONArray results = this.dados.getJSONArray("results");
            JSONObject resultArray = results.getJSONObject(0);
            JSONArray adress = resultArray.getJSONArray("address_components");
            for (int i = 0; i<adress.length();i++) {
                JSONObject ende = adress.getJSONObject(i);
                //this.checkTypeEndereco(ende.getString("type"),ende.getString("long_name"), ende.getString("short_name"));
            }
            this.adress = resultArray;
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void checkTypeEndereco(String type, String long_name, String short_name){
        switch (type) {
            case "street_number" :
                this.numero = long_name;
            break;
            case "route" :
                this.rua = long_name;
                break;
            case "sublocality" :
                this.bairro = long_name;
                break;
            case "country" :
                this.numero = long_name;
                break;
            case "postal_code" :
                this.cep = long_name;
                break;
            case "locality" :
                this.cep = long_name;
                break;
        }
    }
    public JSONObject getAdress() {
        return adress;
    }

    public boolean isError(){
        return status;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getRua() {
        return rua;
    }

    public void setRua(String rua) {
        this.rua = rua;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }
}
