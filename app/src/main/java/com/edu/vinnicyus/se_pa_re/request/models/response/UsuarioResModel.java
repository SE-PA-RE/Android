package com.edu.vinnicyus.se_pa_re.request.models.response;

import java.util.ArrayList;

public class UsuarioResModel {

    private int id;
    private String name;
    private String email;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public class Errors{
        private ArrayList<String> name;
        private ArrayList<String> email;
        private ArrayList<String> password;

        public ArrayList<String> getName() {
            return name;
        }

        public ArrayList<String> getEmail() {
            return email;
        }

        public ArrayList<String> getPassword() {
            return password;
        }
    }
}
