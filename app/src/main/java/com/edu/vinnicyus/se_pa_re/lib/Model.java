package com.edu.vinnicyus.se_pa_re.lib;


import android.app.Application;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class Model extends Application {
    @Override
    public void onCreate()
    {
        super.onCreate();
        Realm.init(this);
        RealmConfiguration config = new RealmConfiguration.Builder()
                .name("se-pa-re.realm")
                .schemaVersion(1)
                .build();
        Realm.setDefaultConfiguration(config);
    }
}
