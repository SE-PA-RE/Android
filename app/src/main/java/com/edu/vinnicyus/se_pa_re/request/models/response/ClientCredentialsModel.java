package com.edu.vinnicyus.se_pa_re.request.models.response;

public class ClientCredentialsModel {

    private int client_id;
    private String client_secret;

    public int getClient_id() {
        return client_id;
    }

    public void setClient_id(int client_id) {
        this.client_id = client_id;
    }

    public String getClient_secret() {
        return client_secret;
    }

    public void setClient_secret(String client_secret) {
        this.client_secret = client_secret;
    }
}
