package com.edu.vinnicyus.se_pa_re.request.models.response;

public class ErrorsResponseRes<T> {

    private String message;
    private T errors;
    private String error;

    public ErrorsResponseRes(){

    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getErrors() {
        return errors;
    }

    public void setErrors(T errors) {
        this.errors = errors;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public class ErrorSimples{

    }
}
