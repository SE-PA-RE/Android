package com.edu.vinnicyus.se_pa_re.request.api;

import com.edu.vinnicyus.se_pa_re.request.models.request.CadastroModel;
import com.edu.vinnicyus.se_pa_re.request.models.request.UsuarioModel;
import com.edu.vinnicyus.se_pa_re.request.models.response.CadastroResModel;
import com.edu.vinnicyus.se_pa_re.request.models.response.UsuarioResModel;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface UsuarioInterface {

    @POST("usuario")
    Call<CadastroResModel> addUser(@Body CadastroModel cm);

    @PUT("api/usuario/{id}")
    Call<UsuarioResModel> update(@Path("id") String id, @Body UsuarioModel um);

    @GET("api/usuario/0")
    Call<UsuarioResModel> getUser();
}
