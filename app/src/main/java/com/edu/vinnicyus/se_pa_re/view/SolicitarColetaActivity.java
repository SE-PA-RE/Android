package com.edu.vinnicyus.se_pa_re.view;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.edu.vinnicyus.se_pa_re.R;
import com.edu.vinnicyus.se_pa_re.controllers.ProgressViewController;
import com.edu.vinnicyus.se_pa_re.request.HttpGlobalRetrofit;
import com.edu.vinnicyus.se_pa_re.request.api.SolicitacaoColetaInterface;
import com.edu.vinnicyus.se_pa_re.request.deserializer.SolicitacaoColetaDes;
import com.edu.vinnicyus.se_pa_re.request.models.request.SolicitacaoColetaModel;
import com.edu.vinnicyus.se_pa_re.request.models.response.SolicitacaoColetaResModel;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;

public class SolicitarColetaActivity extends AppCompatActivity implements OnMapReadyCallback, LocationListener {

    private GoogleMap mMap;
    private LatLng latLng;
    private Marker marker;

    private LinearLayout btn_solicitar;
    private View text_solicita;
    private View progress_solicita;

    private AppCompatTextView info_user_text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_solicitar_coleta);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        info_user_text = findViewById(R.id.info_user_text);
        startGettingLocations();

        btn_solicitar = findViewById(R.id.btn_solicita);
        text_solicita = findViewById(R.id.text_info);
        progress_solicita = findViewById(R.id.progress_solicita);

        btn_solicitar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendSolicitacao();
            }
        });

    }

    public SolicitacaoColetaModel getLocation(){
        return new SolicitacaoColetaModel(marker.getPosition().latitude,marker.getPosition().longitude);
    }
    public void sendSolicitacao(){
        if(marker != null) {
            SolicitacaoColeta(getLocation());
        }else{
            mensagem("Localizacao ainda nao definida!");
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.toolbar_solicita_coleta, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_location:
                startGettingLocations();
                return true;
            case android.R.id.home:
                finish();
                return true;
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }
    public void SolicitacaoColeta(SolicitacaoColetaModel locations) {

            final ProgressViewController pc = new ProgressViewController(text_solicita, progress_solicita, getResources());
            pc.show(true);


                Gson gson  = new GsonBuilder().registerTypeAdapter( SolicitacaoColetaResModel.class, new SolicitacaoColetaDes()).create();

                HttpGlobalRetrofit retrofit = new HttpGlobalRetrofit();

                SolicitacaoColetaInterface solicitacaoColetaInterface = retrofit.getRetrofit().create(SolicitacaoColetaInterface.class);

                Call<SolicitacaoColetaResModel> call = solicitacaoColetaInterface.solicitarColeta(locations);
                call.enqueue(new Callback<SolicitacaoColetaResModel>() {
                    @Override
                    public void onResponse(Call<SolicitacaoColetaResModel> call, retrofit2.Response<SolicitacaoColetaResModel> response) {
                        SolicitacaoColetaResModel solicitacaoColetaModel = null;
                        if(response.code() == 200) {
                            mensagem(response.body().getMessage());
                            if(response.body().isStatus()){
                                Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        next();
                                    }
                                }, 1000);
                            }
                        }else if (response.code() == 422) {
                            Gson gson = new Gson();
                            solicitacaoColetaModel = gson.fromJson(response.errorBody().charStream(), SolicitacaoColetaResModel.class);
                        }
                        pc.show(false);
                        }

                    @Override
                    public void onFailure(Call<SolicitacaoColetaResModel> call, Throwable t) {
                        Log.i("app", "");
                        pc.show(false);
                    }
                });

    }

    public void next(){
        finish();
    }

    public void mensagem(String text){
        Toast.makeText(this,text,Toast.LENGTH_LONG).show();
    }
    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
    }

    @Override
    public void onLocationChanged(Location location) {
        info_user_text.setVisibility(View.GONE);
        if (marker != null){
            marker.remove();
        }

        latLng = new LatLng(location.getLatitude(), location.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title("Localização Atual");
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
        marker = mMap.addMarker(markerOptions);

        CameraPosition cameraPosition = new CameraPosition.Builder().zoom(15).target(latLng).build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }


    private ArrayList findUnAskedPermissions(ArrayList<String> wanted) {
        ArrayList result = new ArrayList();

        for (String perm : wanted) {
            if (!hasPermission(perm)) {
                result.add(perm);
            }
        }

        return result;
    }

    private boolean hasPermission(String permission) {
        if (canAskPermission()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    }

    private boolean canAskPermission() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("GPS desativado!");
        alertDialog.setMessage("Ativar GPS?");
        alertDialog.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        });

        alertDialog.setNegativeButton("Não", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        alertDialog.show();
    }


    private void startGettingLocations() {
        info_user_text.setVisibility(View.VISIBLE);
        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        boolean isGPS = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean isNetwork = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        boolean canGetLocation = true;
        int ALL_PERMISSIONS_RESULT = 101;
        long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10;// Distance in meters
        long MIN_TIME_BW_UPDATES = 1000 * 10;// Time in milliseconds

        ArrayList<String> permissions = new ArrayList<>();
        ArrayList<String> permissionsToRequest;

        permissions.add(android.Manifest.permission.ACCESS_FINE_LOCATION);
        permissions.add(android.Manifest.permission.ACCESS_COARSE_LOCATION);
        permissionsToRequest = findUnAskedPermissions(permissions);

        //Check if GPS and Network are on, if not asks the user to turn on
        if (!isGPS && !isNetwork) {
            showSettingsAlert();
        } else {
            // check permissions

            // check permissions for later versions
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (permissionsToRequest.size() > 0) {
                    requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]),
                            ALL_PERMISSIONS_RESULT);
                    canGetLocation = false;
                }
            }
        }


        //Checks if FINE LOCATION and COARSE Location were granted
        if (ActivityCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {

            Toast.makeText(this, "Permissão negada", Toast.LENGTH_SHORT).show();
            return;
        }

        //Starts requesting location updates
        if (canGetLocation) {
            if (isGPS) {
                lm.requestLocationUpdates(
                        LocationManager.GPS_PROVIDER,
                        MIN_TIME_BW_UPDATES,
                        MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

            } else if (isNetwork) {
                // from Network Provider

                lm.requestLocationUpdates(
                        LocationManager.NETWORK_PROVIDER,
                        MIN_TIME_BW_UPDATES,
                        MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

            }

        } else {
            Toast.makeText(this, "Não é possível obter a localização", Toast.LENGTH_SHORT).show();
        }
    }

//    private void getMarkers(){
//
//        mDatabase.child("location").addListenerForSingleValueEvent(
//                new ValueEventListener() {
//                    @Override
//                    public void onDataChange(DataSnapshot dataSnapshot) {
//                        //Get map of users in datasnapshot
//                        if (dataSnapshot.getValue() != null)
//                            getAllLocations((Map<String,Object>) dataSnapshot.getValue());
//                    }
//
//                    @Override
//                    public void onCancelled(DatabaseError databaseError) {
//                        //handle databaseError
//                    }
//                });
//    }
//
//    private void getAllLocations(Map<String,Object> locations) {
//
//
//
//
//        for (Map.Entry<String, Object> entry : locations.entrySet()){
//
//            Date newDate = new Date(Long.valueOf(entry.getKey()));
//            Map singleLocation = (Map) entry.getValue();
//            LatLng latLng = new LatLng((Double) singleLocation.get("latitude"), (Double)singleLocation.get("longitude"));
//            addGreenMarker(newDate, latLng);
//
//        }
//
//
//    }
//
//    private void addGreenMarker(Date newDate, LatLng latLng) {
//        SimpleDateFormat dt = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
//        MarkerOptions markerOptions = new MarkerOptions();
//        markerOptions.position(latLng);
//        markerOptions.title(dt.format(newDate));
//        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
//        mMap.addMarker(markerOptions);
//    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }
}
