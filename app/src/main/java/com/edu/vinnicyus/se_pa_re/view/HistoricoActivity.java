package com.edu.vinnicyus.se_pa_re.view;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.edu.vinnicyus.se_pa_re.R;
import com.edu.vinnicyus.se_pa_re.controllers.ProgressViewController;
import com.edu.vinnicyus.se_pa_re.request.HttpGlobalRetrofit;
import com.edu.vinnicyus.se_pa_re.request.api.SolicitacaoColetaInterface;
import com.edu.vinnicyus.se_pa_re.request.deserializer.ListSolicitacoesDes;
import com.edu.vinnicyus.se_pa_re.request.models.response.ListSolicitacoesResModel;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


import retrofit2.Call;
import retrofit2.Callback;

public class HistoricoActivity extends AppCompatActivity {

    private TableLayout tl;

    private View form;
    private View progress;

    private ProgressViewController pc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_historico);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        myToolbar.setTitle("Historico");
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        tl = findViewById(R.id.tl_detalhes);

        form = findViewById(R.id.historico_form);
        progress = findViewById(R.id.historico_progress);
        pc = new ProgressViewController(form,progress,getResources());
        getDetails();

    }
    private void getDetails(){
        HistoricoAsync();
    }

    private void setDetails(ListSolicitacoesResModel ls){
        if(ls.getPendentes().size() == 1) {
            TableRow tr = new TableRow(this);
            TextView data = new TextView(this);
            TextView status = new TextView(this);
            TextView agente = new TextView(this);

            data.setText(ls.getPendentes().get(0).getCreated_at());
            data.setTextColor(getResources().getColor(R.color.primaryText));
            status.setText(ls.getPendentes().get(0).getStatus());
            status.setTextColor(getResources().getColor(R.color.primaryText));
            status.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            status.setTextColor(getColor(ls.getPendentes().get(0).getStatus()));

//            Button btn = new Button(this);
//            btn.setTextColor(getResources().getColor(R.color.colorAccent));
//            btn.setTextSize(getResources().getDimension(R.dimen.home_activity_btn_table));
//            btn.setBackground(getDrawable(R.drawable.btn_home_activity_table));
//            btn.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//
//                }
//            });
//            btn.setText("+");

            tr.addView(data);
            tr.addView(agente);
            tr.addView(status);
            //tr.addView(btn);

            tl.addView(tr);
        }
        for (int i=0;i<ls.getFinalizados_emAtendimento().size();i++) {
            TableRow tr = new TableRow(this);

            TextView data = new TextView(this);
            TextView status = new TextView(this);
            TextView agente = new TextView(this);

            data.setText(ls.getFinalizados_emAtendimento().get(i).getCreated_at());
            data.setTextColor(getResources().getColor(R.color.primaryText));
            status.setText(ls.getFinalizados_emAtendimento().get(i).getStatus());
            status.setTextColor(getResources().getColor(R.color.primaryText));
            status.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            status.setTextColor(getColor(ls.getFinalizados_emAtendimento().get(i).getStatus()));
            agente.setText(ls.getFinalizados_emAtendimento().get(i).getColeta().getAgente().getUsuario().getName());
            agente.setTextColor(getResources().getColor(R.color.primaryText));

//            Button btn = new Button(this);
//            btn.setTextColor(getResources().getColor(R.color.colorAccent));
//            btn.setTextSize(getResources().getDimension(R.dimen.home_activity_btn_table));
//            btn.setBackground(getDrawable(R.drawable.btn_home_activity_table));
//            btn.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//
//                }
//            });
//            btn.setText("+");

            tr.addView(data);
            tr.addView(agente);
            tr.addView(status);
            //tr.addView(btn);

            tl.addView(tr);

        }
        pc.show(false);
    }

    private int getColor(String status){
        int color = 0;
        switch (status){
            case "Pendente" :
                color = getResources().getColor(R.color.colorAccent);
                break;
            case "Finalizado":
                color = getResources().getColor(R.color.red);
                break;
            case "Em Atendimento":
                color = getResources().getColor(R.color.colorPrimaryDark);
                break;
        }
        return color;
    }

    public void HistoricoAsync() {
        pc.show(true);
                Gson gson  = new GsonBuilder().registerTypeAdapter( ListSolicitacoesResModel.class, new ListSolicitacoesDes()).create();

                HttpGlobalRetrofit retrofit = new HttpGlobalRetrofit();

                SolicitacaoColetaInterface tokenInterface = retrofit.getRetrofit().create(SolicitacaoColetaInterface.class);

                Call<ListSolicitacoesResModel> call = tokenInterface.getSolicitacoes();
                call.enqueue(new Callback<ListSolicitacoesResModel>() {
                    @Override
                    public void onResponse(Call<ListSolicitacoesResModel> call, retrofit2.Response<ListSolicitacoesResModel> response) {
                        if(response.code() == 200){
                            setDetails(response.body());
                        }else if(response.code() == 422){
                            mensagem("Falha no Servidor!");
                            pc.show(false);
                        }else{}
                    }

                    @Override
                    public void onFailure(Call<ListSolicitacoesResModel> call, Throwable t) {
                        //Toast.makeText(this, "Falha de conexão", Toast.LENGTH_LONG).show();
                        pc.show(false);
                    }
                });

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId() == android.R.id.home){
            finish();
        }

        return true;
    }

    public void mensagem(String s){
        Toast.makeText(this,s, Toast.LENGTH_SHORT).show();
    }
}
