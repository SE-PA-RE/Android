package com.edu.vinnicyus.se_pa_re;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.edu.vinnicyus.se_pa_re.request.HttpGlobalRetrofit;
import com.edu.vinnicyus.se_pa_re.request.api.UsuarioInterface;
import com.edu.vinnicyus.se_pa_re.request.deserializer.CadastroDes;
import com.edu.vinnicyus.se_pa_re.request.models.request.CadastroModel;
import com.edu.vinnicyus.se_pa_re.request.models.response.CadastroResModel;
import com.edu.vinnicyus.se_pa_re.validator.CadastroUsuario;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Call;
import retrofit2.Callback;


public class CadastroFragment extends Fragment{

    private EditText nome;
    private EditText password;
    private EditText email;

    private View mProgressBar;
    private View mFormBar;
    private CadastroUsuario cu;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cadastro, container, false);

        nome = view.findViewById(R.id.nome);
        password = view.findViewById(R.id.password);
        email = view.findViewById(R.id.email);

        mProgressBar = view.findViewById(R.id.cadastro_progress);
        mFormBar = view.findViewById(R.id.form_cadastro);
        Button mEmailSignInButton = view.findViewById(R.id.button_cadastro);
        mEmailSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showProgress(true);
                ((InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE))
                        .hideSoftInputFromWindow(password.getWindowToken(), 0);
                attempCadastro();
            }
        });
        //requestEndereco();
        return view;
    }

    public boolean temInternet() {
        ConnectivityManager manager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = manager.getActiveNetworkInfo();
        return info != null && info.isConnected();
    }
    public void requestEndereco() {
        if(temInternet()) {

        }else{
            mensagem("Sem conexão com a Internet!");
        }
    }
    public void mensagem(String m)
    {
        Toast.makeText(getContext(), m, Toast.LENGTH_SHORT).show();
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mFormBar.setVisibility(show ? View.GONE : View.VISIBLE);
            mFormBar.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mFormBar.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressBar.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressBar.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressBar.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressBar.setVisibility(show ? View.VISIBLE : View.GONE);
            mFormBar.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }

    private void attempCadastro(){

        String login = email.getText().toString();
        String senha = password.getText().toString();

        nome.setError(null);
        email.setError(null);
        password.setError(null);

        boolean cancel = false;
        View focusView = null;

        if(!isEmailValid(login)){
            cancel = true;
            focusView = email;
            email.setError(getString(R.string.error_invalid_email));
        }
        if(!isPasswordValid(senha)){
            cancel = true;
            focusView = password;
            password.setError(getString(R.string.error_invalid_password));
        }
        if(nome.getText().toString().equals("")){
            cancel = true;
            focusView = nome;
            nome.setError(getString(R.string.error_nome_vazio));
        }

        if(cancel){
            showProgress(false);
            focusView.requestFocus();
        }else{
            sendCadastro();
        }
    }
    public void sendCadastro(){
        CadastroModel cm = new CadastroModel(nome.getText().toString(), email.getText().toString(), password.getText().toString(), "Usuario");
        Gson gson = new GsonBuilder().registerTypeAdapter(CadastroResModel.class, new CadastroDes()).create();

        HttpGlobalRetrofit retrofit = new HttpGlobalRetrofit();

        UsuarioInterface usuarioInterface = retrofit.getRetrofit().create(UsuarioInterface.class);

        Call<CadastroResModel> call = usuarioInterface.addUser(cm);
        call.enqueue(new Callback<CadastroResModel>() {
            @Override
            public void onResponse(Call<CadastroResModel> call, retrofit2.Response<CadastroResModel> response) {
                CadastroResModel cadastro = null;
                if (response.code() == 200) {
                    cu = new CadastroUsuario(nome, password, email, response.body());
                    mensagem(cu.getMessage());

                } else {
                    if (response.code() == 422) {
                        Gson gson = new Gson();
                        cadastro = gson.fromJson(response.errorBody().charStream(), CadastroResModel.class);
                        cadastro.setStatus(false);
                        cu = new CadastroUsuario(nome, password, email, cadastro);
                    } else {
                        Toast.makeText(getContext(), "Error", Toast.LENGTH_LONG).show();
                    }
                }
                showProgress(false);
            }

            @Override
            public void onFailure(Call<CadastroResModel> call, Throwable t) {
                Log.i("app", "");
                showProgress(false);
            }
        });
    }
    private void login(){

    }
}
