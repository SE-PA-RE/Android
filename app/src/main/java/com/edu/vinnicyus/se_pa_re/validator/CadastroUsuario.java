package com.edu.vinnicyus.se_pa_re.validator;

import android.widget.EditText;

import com.edu.vinnicyus.se_pa_re.request.models.response.CadastroResModel;
public class CadastroUsuario {

    private boolean status;

    private EditText nome;
    private EditText senha;
    private EditText login;

    private CadastroResModel response;

    private String message;

    public CadastroUsuario(EditText nome, EditText senha, EditText login, CadastroResModel response){

        this.nome = nome;
        this.senha = senha;
        this.login = login;
        this.response = response;
        validar();
    }


    public void validar(){
            if (response.isStatus()) {
                setStatus(true);
                nome.setText("");
                senha.setText("");
                login.setText("");
                setMessage("Cadastro realizado com sucesso!");
            } else {
                nome.setError( response.getErrors().getName() != null ? response.getErrors().getName().get(0).toString() : null);
                login.setError( response.getErrors().getEmail() != null ? response.getErrors().getEmail().get(0).toString() : null);
                senha.setError( response.getErrors().getPassword() != null ? response.getErrors().getPassword().get(0).toString() : null);

                setMessage("Alguns campos encontram-se incorretos!");
                setStatus(false);
            }
        //check();
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public EditText getNome() {
        return nome;
    }

    public void setNome(EditText nome) {
        this.nome = nome;
    }

    public EditText getSenha() {
        return senha;
    }

    public void setSenha(EditText senha) {
        this.senha = senha;
    }

    public EditText getLogin() {
        return login;
    }

    public void setLogin(EditText login) {
        this.login = login;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
