package com.edu.vinnicyus.se_pa_re.request.api;

import com.edu.vinnicyus.se_pa_re.request.models.response.ClientCredentialsModel;
import com.edu.vinnicyus.se_pa_re.request.models.request.GetTokenModel;
import com.edu.vinnicyus.se_pa_re.request.models.response.GetTokenResModel;


import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface TokenInterface {

    @GET("credentials_client")
    Call<ClientCredentialsModel> getCredentials();

    @POST("oauth/token")
    Call<GetTokenResModel> createToken(@Body GetTokenModel getToken);

}
