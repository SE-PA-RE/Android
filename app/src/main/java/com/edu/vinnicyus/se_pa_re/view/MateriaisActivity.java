package com.edu.vinnicyus.se_pa_re.view;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.edu.vinnicyus.se_pa_re.R;
import com.edu.vinnicyus.se_pa_re.controllers.ProgressViewController;
import com.edu.vinnicyus.se_pa_re.request.HttpGlobalRetrofit;
import com.edu.vinnicyus.se_pa_re.request.api.ColetaInterface;
import com.edu.vinnicyus.se_pa_re.request.deserializer.MaterialUnidadeDes;
import com.edu.vinnicyus.se_pa_re.request.deserializer.StoreColetaDes;
import com.edu.vinnicyus.se_pa_re.request.models.request.StoreColetaModel;
import com.edu.vinnicyus.se_pa_re.request.models.response.MaterialUnidadeResModel;
import com.edu.vinnicyus.se_pa_re.request.models.response.StoreColetaRes;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;


public class MateriaisActivity extends AppCompatActivity implements LocationListener {

    private View falha;
    private View form;
    private View progress;
    private ProgressViewController pc;
    private MaterialUnidadeResModel materiais_unidades;
    private StoreColetaModel sc;
    private ArrayList<String> materiais = new ArrayList<String>();
    private ArrayList<String> unidades = new ArrayList<String>();
    private LatLng latLng;
    private LocationManager lm;

    private Spinner spinner1;
    private Spinner spinner2;
    private Spinner spinner3;
    private Spinner spinner4;
    private Spinner spinner5;
    private Spinner spinner6;
    private Spinner spinner7;
    private Spinner spinner8;

    private AppCompatEditText qntd1;
    private AppCompatEditText qntd2;
    private AppCompatEditText qntd3;
    private AppCompatEditText qntd4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_materiais);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        myToolbar.setTitle("Recebimento de Materiais");
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        falha = findViewById(R.id.falha);
        form = findViewById(R.id.form);
        progress = findViewById(R.id.progress);

        pc = new ProgressViewController(form,progress,falha,getResources());
        falha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pc.falha(false);
                if(materiais_unidades == null) {
                    getMateriasiUnidade();
                }else{
                    pc.show(false);
                    Toast.makeText(getApplicationContext(),"Falha de conexão, tente novamente!",Toast.LENGTH_LONG).show();
                }
            }
        });

        spinner1 = findViewById(R.id.spinner1);
        spinner2 = findViewById(R.id.spinner2);
        spinner3 = findViewById(R.id.spinner3);
        spinner4 = findViewById(R.id.spinner4);
        spinner5 = findViewById(R.id.spinner5);
        spinner6 = findViewById(R.id.spinner6);
        spinner7 = findViewById(R.id.spinner7);
        spinner8 = findViewById(R.id.spinner8);

        qntd1 = findViewById(R.id.qntd1);
        qntd2 = findViewById(R.id.qntd2);
        qntd3 = findViewById(R.id.qntd3);
        qntd4 = findViewById(R.id.qntd4);
        // Spinner click listener

        AppCompatButton btn_salvar = findViewById(R.id.btn_salvar);
        btn_salvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pc.show(true);
                startGettingLocations();
            }
        });

        sc = new StoreColetaModel();
    }

    private void validar(){
        boolean test = false;
        if(!spinner1.getSelectedItem().toString().equals("Selecionar...") || !TextUtils.isEmpty(qntd1.getText().toString())) {
            if (TextUtils.isEmpty(qntd1.getText().toString())) {
                qntd1.setError("Valor Obrigatorio!");
                test=true;
            }else if(spinner1.getSelectedItem().toString().equals("Selecionar...")) {
                TextView tv1= findViewById(R.id.txv1);
                tv1.setVisibility(View.VISIBLE);
                test=true;
            }else{
                sc.getColeta().add(sc.new Coleta(spinner1.getSelectedItem().toString(), spinner2.getSelectedItem().toString(), Float.parseFloat(qntd1.getText().toString())));
            }
        }

        if(!spinner3.getSelectedItem().toString().equals("Selecionar...") || !TextUtils.isEmpty(qntd2.getText().toString())) {
            if (TextUtils.isEmpty(qntd2.getText().toString())) {
                qntd2.setError("Valor Obrigatorio!");
                test=true;
            }else if(spinner3.getSelectedItem().toString().equals("Selecionar...")) {
                TextView tv1= findViewById(R.id.txv2);
                tv1.setVisibility(View.VISIBLE);
                test=true;
            }else{
                sc.getColeta().add(sc.new Coleta(spinner3.getSelectedItem().toString(), spinner4.getSelectedItem().toString(), Float.parseFloat(qntd2.getText().toString())));
            }
        }

        if(!spinner5.getSelectedItem().toString().equals("Selecionar...") || !TextUtils.isEmpty(qntd3.getText().toString())) {
            if (TextUtils.isEmpty(qntd3.getText().toString())) {
                qntd3.setError("Valor Obrigatorio!");
                test=true;
            }else if(spinner5.getSelectedItem().toString().equals("Selecionar...")) {
                TextView tv1= findViewById(R.id.txv3);
                tv1.setVisibility(View.VISIBLE);
                test=true;
            }else{
                sc.getColeta().add(sc.new Coleta(spinner5.getSelectedItem().toString(), spinner6.getSelectedItem().toString(), Float.parseFloat(qntd3.getText().toString())));
            }
        }

        if(!spinner7.getSelectedItem().toString().equals("Selecionar...") || !TextUtils.isEmpty(qntd4.getText().toString())) {
            if (TextUtils.isEmpty(qntd4.getText().toString())) {
                qntd4.setError("Valor Obrigatorio!");
                test=true;
            }else if(spinner7.getSelectedItem().toString().equals("Selecionar...")) {
                TextView tv1= findViewById(R.id.txv4);
                tv1.setVisibility(View.VISIBLE);
                test=true;
            }else{
                sc.getColeta().add(sc.new Coleta(spinner7.getSelectedItem().toString(), spinner8.getSelectedItem().toString(), Float.parseFloat(qntd4.getText().toString())));
            }
        }

        if(test){
            pc.show(false);
        }else {
            store(sc);
        }
    }

    private void store(StoreColetaModel scm){
        Gson gson = new GsonBuilder().registerTypeAdapter(StoreColetaRes.class, new StoreColetaDes()).create();

        HttpGlobalRetrofit retrofit = new HttpGlobalRetrofit(gson);

        ColetaInterface tokenInterface = retrofit.getRetrofit().create(ColetaInterface.class);

        Call<StoreColetaRes> call = tokenInterface.storeColeta(scm);
        call.enqueue(new Callback<StoreColetaRes>() {
            @Override
            public void onResponse(Call<StoreColetaRes> call, retrofit2.Response<StoreColetaRes> response) {
                if (response.code() == 200) {
                    pc.show(false);
                    Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    resetar();
                } else if (response.code() == 422) {
                    //finish();
                    pc.show(false);
                } else {
                    pc.falha(true);
                }

            }

            @Override
            public void onFailure(Call<StoreColetaRes> call, Throwable t) {
                pc.falha(true);
            }
        });
    }
    private void resetar(){
        qntd1.setText(null);
        qntd2.setText(null);
        qntd3.setText(null);
        qntd4.setText(null);

        spinner1.setSelection(0);
        spinner3.setSelection(0);
        spinner5.setSelection(0);
        spinner7.setSelection(0);

        sc = new StoreColetaModel();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            finish();
        }
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        pc.show(true);
        getMateriasiUnidade();
    }

    private void getMateriasiUnidade(){
        Gson gson = new GsonBuilder().registerTypeAdapter(MaterialUnidadeResModel.class, new MaterialUnidadeDes()).create();

        HttpGlobalRetrofit retrofit = new HttpGlobalRetrofit(gson);

        ColetaInterface tokenInterface = retrofit.getRetrofit().create(ColetaInterface.class);

        Call<MaterialUnidadeResModel> call = tokenInterface.getMaterialUnidade();
        call.enqueue(new Callback<MaterialUnidadeResModel>() {
            @Override
            public void onResponse(Call<MaterialUnidadeResModel> call, retrofit2.Response<MaterialUnidadeResModel> response) {
                if (response.code() == 200) {
                    pc.show(false);
                    materiais_unidades = response.body();
                    setMateriaisUnidade();
                } else if (response.code() == 422) {
                    //finish();
                    pc.show(false);
                } else {
                    pc.falha(true);
                }

            }

            @Override
            public void onFailure(Call<MaterialUnidadeResModel> call, Throwable t) {
                pc.falha(true);
            }
        });
    }

    private void setMateriaisUnidade(){

        materiais.add("Selecionar...");
        for (int i=0; i<materiais_unidades.getMaterial().size();i++){
            materiais.add(materiais_unidades.getMaterial().get(i).getMaterial());
        }

        for (int i=0; i<materiais_unidades.getUnidades().size();i++){
            unidades.add(materiais_unidades.getUnidades().get(i).getTipo());
        }

        ArrayAdapter<String> listmateriais = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, materiais);
        listmateriais.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        ArrayAdapter<String> listunidades = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, unidades);
        listunidades.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner1.setAdapter(listmateriais);

        spinner3.setAdapter(listmateriais);

        spinner5.setAdapter(listmateriais);

        spinner7.setAdapter(listmateriais);

        spinner2.setAdapter(listunidades);

        spinner4.setAdapter(listunidades);

        spinner6.setAdapter(listunidades);

        spinner8.setAdapter(listunidades);

    }

    @Override
    public void onLocationChanged(Location location) {
        latLng = new LatLng(location.getLatitude(), location.getLongitude());
        sc.setLocalidade(latLng.latitude,latLng.longitude);
        validar();
        lm.removeUpdates(this);
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }


    private ArrayList findUnAskedPermissions(ArrayList<String> wanted) {
        ArrayList result = new ArrayList();

        for (String perm : wanted) {
            if (!hasPermission(perm)) {
                result.add(perm);
            }
        }

        return result;
    }

    private boolean hasPermission(String permission) {
        if (canAskPermission()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    }

    private boolean canAskPermission() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("GPS desativado!");
        alertDialog.setMessage("Ativar GPS?");
        alertDialog.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        });

        alertDialog.setNegativeButton("Não", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        alertDialog.show();
    }

    private void startGettingLocations() {

        lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        boolean isGPS = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean isNetwork = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        boolean canGetLocation = true;
        int ALL_PERMISSIONS_RESULT = 101;
        long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10;// Distance in meters
        long MIN_TIME_BW_UPDATES = 1000 * 10;// Time in milliseconds

        ArrayList<String> permissions = new ArrayList<>();
        ArrayList<String> permissionsToRequest;

        permissions.add(android.Manifest.permission.ACCESS_FINE_LOCATION);
        permissions.add(android.Manifest.permission.ACCESS_COARSE_LOCATION);
        permissionsToRequest = findUnAskedPermissions(permissions);

        //Check if GPS and Network are on, if not asks the user to turn on
        if (!isGPS && !isNetwork) {
            showSettingsAlert();
        } else {
            // check permissions

            // check permissions for later versions
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (permissionsToRequest.size() > 0) {
                    requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]),
                            ALL_PERMISSIONS_RESULT);
                    canGetLocation = false;
                }
            }
        }


        //Checks if FINE LOCATION and COARSE Location were granted
        if (ActivityCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(this, "Permissão negada", Toast.LENGTH_SHORT).show();
            return;
        }

        //Starts requesting location updates
        if (canGetLocation) {
            if (isGPS) {
                lm.requestLocationUpdates(
                        LocationManager.GPS_PROVIDER,
                        MIN_TIME_BW_UPDATES,
                        MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

            } else if (isNetwork) {
                // from Network Provider

                lm.requestLocationUpdates(
                        LocationManager.NETWORK_PROVIDER,
                        MIN_TIME_BW_UPDATES,
                        MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

            }
        } else {
            Toast.makeText(this, "Não é possível obter a localização", Toast.LENGTH_SHORT).show();
        }
    }

}
