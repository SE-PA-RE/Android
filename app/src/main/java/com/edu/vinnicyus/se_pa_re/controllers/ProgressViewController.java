package com.edu.vinnicyus.se_pa_re.controllers;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.res.Resources;
import android.os.Build;
import android.view.View;

public class ProgressViewController {

    private View form;
    private View progress;
    private Resources resources;
    private View falha;

    public ProgressViewController(View form, View progress, View falha, Resources resources){
        this.form = form;
        this.progress = progress;
        this.falha = falha;
        this.resources = resources;
    }

    public ProgressViewController(View form, View progress, Resources resources) {
        this.form = form;
        this.progress = progress;
        this.resources = resources;
    }

    public void show(boolean b){
        this.showProgress(b);
    }
    public void falha(boolean b){
        this.gfalha(b);
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = resources.getInteger(android.R.integer.config_shortAnimTime);

            form.setVisibility(show ? View.GONE : View.VISIBLE);
            form.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    form.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            progress.setVisibility(show ? View.VISIBLE : View.GONE);
            progress.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    progress.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            progress.setVisibility(show ? View.VISIBLE : View.GONE);
            form.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }
    private void gfalha(final boolean show){
        progress.setVisibility(show ? View.GONE : View.VISIBLE);
        falha.setVisibility(show ? View.VISIBLE : View.GONE);
    }
}
