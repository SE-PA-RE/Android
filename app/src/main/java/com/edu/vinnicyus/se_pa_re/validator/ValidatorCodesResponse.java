package com.edu.vinnicyus.se_pa_re.validator;

import com.edu.vinnicyus.se_pa_re.request.HttpRefreshToken;
import com.edu.vinnicyus.se_pa_re.request.models.response.ErrorsResponseRes;
import com.google.gson.Gson;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

public class ValidatorCodesResponse<T,E> {

    private retrofit2.Response<T> response;
    private T value;
    private ErrorsResponseRes<E> errors;
    private int status;
    private Class<?> dataerror;

    public ValidatorCodesResponse(retrofit2.Response<T> r, final Class<?> dataError){
        this.response = r;
        this.dataerror = dataError;
        this.status = 2;
    }

    public ValidatorCodesResponse(retrofit2.Response<T> r){
        this.response = r;
        this.status = 2;
    }
    /* Status Config
        0 = false;
        1 = true;
        2 = error.request;
        3 = Unauthenticated
     */
    public void execute(){
        if(response != null) {
            switch ((int) response.code()) {
                case 200:
                    value = response.body();
                    this.status = 1;
                    break;
                case 422:
                    Gson gson = new Gson();
                    errors = gson.fromJson(response.errorBody().charStream(), getType(ErrorsResponseRes.class, dataerror));
                    this.status = 0;
                    break;
                case 401:
                    this.status = 3;
                    refreshToken();
                    break;
                default:
                    this.status = 2;
                    break;
            }
        }
    }

    public T getValue() {
        return value;
    }

    public ErrorsResponseRes<E> getErrors() {
        return errors;
    }

    public int isStatus() {
        return status;
    }

    public static Type getType(final Class<?> rawClass,final Class<?> parameter) {
        return new ParameterizedType() {
            @Override
            public Type[] getActualTypeArguments() {
                return new Type[] {parameter};
            }
            @Override
            public Type getRawType() {
                return rawClass;
            }
            @Override
            public Type getOwnerType() {
                return null;
            }
        };
    }

    private void refreshToken(){
        HttpRefreshToken httpRefreshToken = new HttpRefreshToken();
    }
}
