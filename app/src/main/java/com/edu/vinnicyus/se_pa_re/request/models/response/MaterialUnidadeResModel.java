package com.edu.vinnicyus.se_pa_re.request.models.response;

import java.util.ArrayList;

public class MaterialUnidadeResModel {

    private ArrayList<Material> material;
    private ArrayList<Unidade> unidades;

    public ArrayList<Material> getMaterial() {
        return material;
    }

    public ArrayList<Unidade> getUnidades() {
        return unidades;
    }

    public class Material{

        private int id;
        private String material;
        private String descricao;

        public int getId() {
            return id;
        }

        public String getMaterial() {
            return material;
        }

        public String getDescricao() {
            return descricao;
        }
    }

    public class Unidade{

        private int id;
        private String tipo;

        public int getId() {
            return id;
        }

        public String getTipo() {
            return tipo;
        }
    }
}
