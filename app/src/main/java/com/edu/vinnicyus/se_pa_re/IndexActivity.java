package com.edu.vinnicyus.se_pa_re;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;

import com.edu.vinnicyus.se_pa_re.models.Token;
import com.edu.vinnicyus.se_pa_re.view.HomeActivity;

import io.realm.Realm;

public class IndexActivity extends AppCompatActivity {

    private TextView mTextMessage;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment select = null;

                switch (item.getItemId()) {
                    case R.id.navigation_home:
                        select = new HomeFragment();
                        break;
                    case R.id.navigation_dashboard:
                        select = new CadastroFragment();
                        break;
                    case R.id.navigation_notifications:
                        select = new LoginFragment();
                        break;

            }
            getSupportFragmentManager().beginTransaction().setCustomAnimations(android.R.anim.fade_in,android.R.anim.fade_out).replace(R.id.fragments, select).addToBackStack(null).commit();

            return true;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_index);
        setTitle("SE-PA-RE");

        Realm realm = Realm.getDefaultInstance();
        Token teste = Token.getLoginAttemp();
        if(teste == null) {

            realm.beginTransaction();
            Token token = realm.createObject(Token.class);
            token.setIdentificar(2);
            realm.commitTransaction();
            realm.close();
        }else{
            if(teste.getAccess_token() != null){
                Intent intent = new Intent(this,
                        HomeActivity.class);
                realm.close();
                startActivity(intent);
                finish();
            }
        }

        if (findViewById(R.id.fragments) != null) {

            // However, if we're being restored from a previous state,
            // then we don't need to do anything and should return or else
            // we could end up with overlapping fragments.
            if (savedInstanceState != null) {
                return;
            }

            // Create a new Fragment to be placed in the activity layout
            HomeFragment firstFragment = new HomeFragment();

            // In case this activity was started with special instructions from an
            // Intent, pass the Intent's extras to the fragment as arguments
            firstFragment.setArguments(getIntent().getExtras());

            // Add the fragment to the 'fragment_container' FrameLayout
            getSupportFragmentManager().beginTransaction().add(R.id.fragments, firstFragment).commit();
        }

        mTextMessage = (TextView) findViewById(R.id.message);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

    }
}
