package com.edu.vinnicyus.se_pa_re.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.edu.vinnicyus.se_pa_re.R;
import com.edu.vinnicyus.se_pa_re.controllers.ProgressViewController;
import com.edu.vinnicyus.se_pa_re.request.HttpGlobalRetrofit;
import com.edu.vinnicyus.se_pa_re.request.api.UsuarioInterface;
import com.edu.vinnicyus.se_pa_re.request.deserializer.UsuarioDes;
import com.edu.vinnicyus.se_pa_re.request.models.request.UsuarioModel;
import com.edu.vinnicyus.se_pa_re.request.models.response.ErrorsResponseRes;
import com.edu.vinnicyus.se_pa_re.request.models.response.UsuarioResModel;
import com.edu.vinnicyus.se_pa_re.validator.ValidatorCodesResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;

public class EditUserActivity extends AppCompatActivity {

    private TextView btn_edit;
    private View progress;
    private View form;

    private EditText tvNome;
    private EditText tvEmail;
    private EditText tvSenha;
    private EditText tvConfirm;

    private ProgressViewController p;


    private UsuarioModel um;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_user);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        myToolbar.setTitle("Minha Conta");
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        btn_edit = findViewById(R.id.edit_usuario);
        progress = findViewById(R.id.progress_user);
        form = findViewById(R.id.form);

        tvNome = findViewById(R.id.nome_text);
        tvEmail = findViewById(R.id.email_text);
        tvSenha = findViewById(R.id.senha);
        tvConfirm = findViewById(R.id.confirm);

        tvNome.setEnabled(false);
        tvEmail.setEnabled(false);
        tvSenha.setEnabled(false);
        tvConfirm.setEnabled(false);

        btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(btn_edit.getText().equals("Salvar")) {
                    validation();
                }else {
                    btn_edit.setText("Salvar");

                    tvNome.setEnabled(true);
                    tvEmail.setEnabled(true);
                    tvSenha.setEnabled(true);
                    tvConfirm.setEnabled(true);
                }
            }
        });

        p = new ProgressViewController(form,progress,getResources());
        GetUser();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId() == android.R.id.home){
            finish();
        }

        return true;
    }

    private void validation(){
        boolean cancel = false;

        if (TextUtils.isEmpty(tvEmail.getText().toString())) {
            tvEmail.setError(getString(R.string.error_field_required));
            cancel = true;
        } else if (!isEmailValid(tvEmail.getText().toString())) {
            tvEmail.setError(getString(R.string.error_invalid_email));
            cancel = true;
        }

        if (TextUtils.isEmpty(tvNome.getText().toString())) {
            tvNome.setError(getString(R.string.error_field_required));
            cancel = true;
        }

        if(TextUtils.isEmpty(tvSenha.getText().toString()) && TextUtils.isEmpty(tvConfirm.getText().toString())){
            cancel = false;
        }else {
            if (!tvConfirm.getText().toString().equals(tvSenha.getText().toString())) {
                tvConfirm.setError("As senhas não sao iguais!");
                cancel = true;
            }else{
                um.setPassword(tvSenha.getText().toString());
                um.setConfim_password(tvConfirm.getText().toString());
            }
        }

        if(!cancel){
            um.setName(tvNome.getText().toString());
            um.setEmail(tvEmail.getText().toString());
            UserUpdate();
        }
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    public void UserUpdate(){

            Realm realm = Realm.getDefaultInstance();

                p.show(true);
                Gson gson  = new GsonBuilder().registerTypeAdapter( UsuarioResModel.class, new UsuarioDes()).create();

                HttpGlobalRetrofit retrofit = new HttpGlobalRetrofit();

                UsuarioInterface usuarioInterface = retrofit.getRetrofit().create(UsuarioInterface.class);

                Call<UsuarioResModel> call = usuarioInterface.update(""+um.getId(), um);
                call.enqueue(new Callback<UsuarioResModel>() {
                    @Override
                    public void onResponse(Call<UsuarioResModel> call, retrofit2.Response<UsuarioResModel> response) {
                        if (response.code() == 200){
                            tvNome.setEnabled(false);
                            tvEmail.setEnabled(false);
                            tvSenha.setEnabled(false);
                            tvConfirm.setEnabled(false);
                            btn_edit.setText("Editar");
                            mensagem("Dados atualizados!");
                        }else if(response.code() == 422){
                            Gson gson = new Gson();
                            ErrorsResponseRes<UsuarioResModel.Errors> usuarioResModel = gson.fromJson(response.errorBody().charStream(), ValidatorCodesResponse.getType(ErrorsResponseRes.class, UsuarioResModel.Errors.class));
                            tvNome.setError( usuarioResModel.getErrors().getName() != null ? usuarioResModel.getErrors().getName().get(0) : null);
                            tvEmail.setError( usuarioResModel.getErrors().getEmail() != null ? usuarioResModel.getErrors().getEmail().get(0) : null);
                            tvSenha.setError( usuarioResModel.getErrors().getPassword() != null ? usuarioResModel.getErrors().getPassword().get(0) : null);
                        }else{
                            //Toast.makeText(getContext(), "Error de Conexão!", Toast.LENGTH_SHORT).show();
                        }
                        p.show(false);
                    }

                    @Override
                    public void onFailure(Call<UsuarioResModel> call, Throwable t) {
                        p.show(false);
                    }
                });
    }
    public void mensagem(String s){
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }
    public void GetUser() {
        p.show(true);
        Gson gson = new GsonBuilder().registerTypeAdapter(UsuarioResModel.class, new UsuarioDes()).create();

        HttpGlobalRetrofit retrofit = new HttpGlobalRetrofit();

        UsuarioInterface tokenInterface = retrofit.getRetrofit().create(UsuarioInterface.class);

        Call<UsuarioResModel> call = tokenInterface.getUser();
        call.enqueue(new Callback<UsuarioResModel>() {
            @Override
            public void onResponse(Call<UsuarioResModel> call, retrofit2.Response<UsuarioResModel> response) {
                if (response.code() == 200) {
                    tvNome.setText(response.body().getName());
                    tvEmail.setText(response.body().getEmail());
                    tvSenha.setText(null);
                    tvConfirm.setText(null);
                    um = new UsuarioModel();
                    um.setId(response.body().getId());
                } else if (response.code() == 422) {
                    finish();
                } else {
                    finish();
                }
                p.show(false);
            }

            @Override
            public void onFailure(Call<UsuarioResModel> call, Throwable t) {

            }
        });
    }


}
